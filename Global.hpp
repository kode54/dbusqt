/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

extern "C" {
#include <sys/types.h>
#include <sys/socket.h>
#include <wlr/types/wlr_idle.h>
};

#include <ctime>
#include <iostream>
#include <set>
#include <list>

#include <unistd.h>
#include <functional>

#include <linux/input-event-codes.h>
#include <linux/input.h>

#include <wayfire/plugin.hpp>

#include <wayfire/compositor-view.hpp>
#include <wayfire/core.hpp>
#include <wayfire/seat.hpp>
#include <wayfire/debug.hpp>
#include <wayfire/img.hpp>
#include <wayfire/input-device.hpp>
#include <wayfire/opengl.hpp>
#include <wayfire/option-wrapper.hpp>
#include <wayfire/output.hpp>
#include <wayfire/output-layout.hpp>
#include <wayfire/render-manager.hpp>
#include <wayfire/util.hpp>
#include <wayfire/signal-definitions.hpp>
#include <wayfire/util/duration.hpp>
#include <wayfire/util/log.hpp>
#include <wayfire/view.hpp>
#include <wayfire/toplevel.hpp>
#include <wayfire/toplevel-view.hpp>
#include <wayfire/workspace-set.hpp>
#include <wayfire/window-manager.hpp>

#include <QString>

/** Global variables */
extern uint focused_view_id;
extern wf::compositor_core_t&      core;
extern std::vector<wf::output_t *> wf_outputs;
extern std::set<wf::output_t *>    connected_wf_outputs;

extern QString service;
extern QString objPath;

wf::output_t * get_output_from_id( uint output_id );
bool check_view_toplevel( wayfire_view view );
wayfire_toplevel_view get_view_from_id( uint view_id );
