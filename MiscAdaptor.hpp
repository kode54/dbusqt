/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include "Global.hpp"
#include "dbustypes.hpp"

class MiscAdaptor : public QDBusAbstractAdaptor {
    Q_OBJECT;
    Q_CLASSINFO( "D-Bus Interface", "wayland.compositor" );

    Q_CLASSINFO( "D-Bus Introspection",
                 "<interface name='wayland.compositor'>\n"
                 "  <signal name='OutputAdded'>\n"
                 "    <arg name='output_id' type='u' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='OutputRemoved'>\n"
                 "    <arg name='output_id' type='u' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='OutputChanged'>\n"
                 "    <arg name='output_id' type='u' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='OutputWorkspaceChanged'>\n"
                 "    <arg name='output_id' type='u' direction='out'/>\n"
                 "    <arg type='(ii)' direction='out'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.Out0' value='WorkSpace'/>\n"
                 "  </signal>\n"
                 "  <signal name='OutputWorkspaceGridChanged'>\n"
                 "    <arg name='output_id' type='u' direction='out'/>\n"
                 "    <arg type='(ii)' direction='out'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.Out0' value='WorkSpaceGrid'/>\n"
                 "  </signal>\n"
                 "  <signal name='ViewAdded'>\n"
                 "    <arg name='view_id' type='u' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='ViewRemoved'>\n"
                 "    <arg name='view_id' type='u' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='ViewAppIdChanged'>\n"
                 "    <arg name='view_id' type='u' direction='out'/>\n"
                 "    <arg name='new_appid' type='s' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='ViewTitleChanged'>\n"
                 "    <arg name='view_id' type='u' direction='out'/>\n"
                 "    <arg name='new_title' type='s' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='ViewAttentionChanged'>\n"
                 "    <arg name='view_id' type='u' direction='out'/>\n"
                 "    <arg name='attention' type='b' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='ViewFocusChanged'>\n"
                 "    <arg name='view_id' type='u' direction='out'/>\n"
                 "    <arg name='has_fovus' type='b' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='ViewFullscreenChanged'>\n"
                 "    <arg name='view_id' type='u' direction='out'/>\n"
                 "    <arg name='fullscreened' type='b' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='ViewStickyChanged'>\n"
                 "    <arg name='view_id' type='u' direction='out'/>\n"
                 "    <arg name='sticky' type='b' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='ViewMaximizedChanged'>\n"
                 "    <arg name='view_id' type='u' direction='out'/>\n"
                 "    <arg name='maximized' type='b' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='ViewMinimizedChanged'>\n"
                 "    <arg name='view_id' type='u' direction='out'/>\n"
                 "    <arg name='minimized' type='b' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='ViewOutputMoved'>\n"
                 "    <arg name='view_id' type='u' direction='out'/>\n"
                 "    <arg name='oldOutput' type='u' direction='out'/>\n"
                 "    <arg name='newOutput' type='u' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='ViewRoleChanged'>\n"
                 "    <arg name='view_id' type='u' direction='out'/>\n"
                 "    <arg name='view_role' type='u' direction='out'/>\n"
                 "  </signal>\n"
                 "  <signal name='ViewWorkspaceChanged'>\n"
                 "    <arg name='view_id' type='u' direction='out'/>\n"
                 "    <arg type='(ii)' direction='out'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.Out0' value='WorkSpace'/>\n"
                 "    <arg type='(ii)' direction='out'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.Out1' value='WorkSpace'/>\n"
                 "  </signal>\n"
                 "  <method name='GetVersion' />"
                 "  <method name='CaptureActiveScreen'>"
                 "    <arg name='filename' type='s' direction='in'/>\n"
                 "  </method>"
                 "  <method name='CaptureActiveView'>\n"
                 "    <arg name='filename' type='s' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='CaptureView'>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "    <arg name='filename' type='s' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='CaptureViewUnderMouse'>\n"
                 "    <arg name='filename' type='s' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='SaveSession'>\n"
                 "    <arg name='name' type='s' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='RestoreSession'>\n"
                 "    <arg name='name' type='s' direction='in'/>\n"
                 "  </method>\n"
                 "</interface>\n"
    );

    public:
        MiscAdaptor( QObject *parent );
        virtual ~MiscAdaptor();

    public Q_SLOTS:
        QString GetVersion();

        void CaptureActiveScreen( QString filename );
        void CaptureActiveView( QString filename );
        void CaptureView( uint view_id, QString filename );
        void CaptureViewUnderMouse( QString filename );

        void SaveSession( QString name );
        void RestoreSession( QString name );

    private:
        uint mOldFocusedViewId = 0;

        /**
         * ================== OUTPUT RELATED
         **/

        /* An output was added - update pager */
        wf::signal::connection_t<wf::output_added_signal> onOutputAdded {
            [ = ] ( wf::output_added_signal *ev ) {
                auto search = connected_wf_outputs.find( ev->output );

                /* If this output already exists */
                if ( search != connected_wf_outputs.end() ) {
                    return;
                }

                ev->output->connect( &onViewMapped );
                ev->output->connect( &onViewMinimized );
                ev->output->connect( &onViewTiled );
                ev->output->connect( &onViewChangeWorkspace );
                ev->output->connect( &onOutputWorkspaceChanged );
                ev->output->connect( &onViewFocused );
                ev->output->connect( &onViewStickied );

                wf_outputs = core.output_layout->get_outputs();
                connected_wf_outputs.insert( ev->output );

                QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "OutputAdded" );

                msg << ev->output->get_id();
                QDBusConnection::sessionBus().send( msg );
            }
        };

        /* An output was removed - update pager */
        wf::signal::connection_t<wf::output_removed_signal> onOutputRemoved {
            [ = ] ( wf::output_removed_signal *ev ) {
                auto search = connected_wf_outputs.find( ev->output );

                if ( search != connected_wf_outputs.end() ) {
                    wf_outputs = core.output_layout->get_outputs();
                    connected_wf_outputs.erase( ev->output );

                    QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "OutputRemoved" );
                    msg << ev->output->get_id();
                    QDBusConnection::sessionBus().send( msg );
                }
            }
        };

        /* An output was removed - update pager */
        wf::signal::connection_t<wf::output_gain_focus_signal> onOutputGainFocus {
            [ = ] ( wf::output_gain_focus_signal *ev ) {
                QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "OutputChanged" );

                msg << ev->output->get_id();
                QDBusConnection::sessionBus().send( msg );
            }
        };

        /* Workspace of an output changed - Update pager */
        wf::signal::connection_t<wf::workspace_changed_signal> onOutputWorkspaceChanged {
            [ = ] ( wf::workspace_changed_signal *ev ) {
                WorkSpace ws;

                ws.row    = ev->new_viewport.y;
                ws.column = ev->new_viewport.x;
                QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "OutputWorkspaceChanged" );

                msg << ev->output->get_id() << QVariant::fromValue( ws );
                QDBusConnection::sessionBus().send( msg );
            }
        };

        /* Workspace of an output changed - Update pager */
        wf::signal::connection_t<wf::workspace_grid_changed_signal> onOutputWorkspaceGridChanged {
            [ = ] ( wf::workspace_grid_changed_signal * ) {
                // wf::dimensions_t grid = ev->new_grid_size;
                //
                // WorkSpaceGrid ws;
                //
                // ws.rows    = grid.height;
                // ws.columns = grid.width;
                //
                // QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor",
                // "OutputWorkspaceGridChanged" );
                //
                // msg << ev->output->get_id() << QVariant::fromValue( ws );
                // QDBusConnection::sessionBus().send( msg );
            }
        };

        /**
         * ================== VIEW RELATED
         **/

        /* A new view was created - add icon to taskbar */
        wf::signal::connection_t<wf::view_mapped_signal> onViewMapped {
            [ = ] ( wf::view_mapped_signal *ev ) {
                /* We're interested only in top-levels */
                if ( ev->view->role != wf::VIEW_ROLE_TOPLEVEL ) {
                    return;
                }

                QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "ViewAdded" );

                msg << ev->view->get_id();
                QDBusConnection::sessionBus().send( msg );

                ev->view->connect( &onViewAppIdChanged );
                ev->view->connect( &onViewTitleChanged );
                ev->view->connect( &onViewUnmapped );
                ev->view->connect( &onViewTiled );
            }
        };

        /* A view was closed - remoe icon from taskbar */
        wf::signal::connection_t<wf::view_unmapped_signal> onViewUnmapped {
            [ = ] ( wf::view_unmapped_signal *ev ) {
                QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "ViewRemoved" );

                msg << ev->view->get_id();
                QDBusConnection::sessionBus().send( msg );
            }
        };

        /* AppId of the view changed - change taskbar icon */
        wf::signal::connection_t<wf::view_app_id_changed_signal> onViewAppIdChanged {
            [ = ] ( wf::view_app_id_changed_signal *ev ) {
                QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "ViewAppIdChanged" );

                msg << ev->view->get_id() << ev->view->get_app_id().c_str();
                QDBusConnection::sessionBus().send( msg );
            }
        };

        /* Title changed - taskbar icon tooltip */
        wf::signal::connection_t<wf::view_title_changed_signal> onViewTitleChanged {
            [ = ] ( wf::view_title_changed_signal *ev ) {
                QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "ViewTitleChanged" );

                msg << ev->view->get_id() << ev->view->get_title().c_str();
                QDBusConnection::sessionBus().send( msg );
            }
        };

        /* Demands Attention changed - flash taskbar icon */
        wf::signal::connection_t<wf::view_hints_changed_signal> onViewHintsChanged {
            [ = ] ( wf::view_hints_changed_signal *ev ) {
                QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "ViewAttentionChanged" );

                msg << ev->view->get_id() << ev->demands_attention;
                QDBusConnection::sessionBus().send( msg );
            }
        };

        /* View was moved from one workspace to another - add/remove taskbar icon */
        wf::signal::connection_t<wf::view_change_workspace_signal> onViewChangeWorkspace {
            [ = ] ( wf::view_change_workspace_signal *ev ) {
                QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "ViewWorkspaceChanged" );

                msg << ev->view->get_id();

                WorkSpace ows;

                ows.row    = ev->from.y;
                ows.column = ev->from.x;

                WorkSpace nws;

                nws.row    = ev->to.y;
                nws.column = ev->to.x;

                msg << QVariant::fromValue( ows ) << QVariant::fromValue( nws );
                QDBusConnection::sessionBus().send( msg );
            }
        };

        /* View was moved from one workspace-set to another - add/remove taskbar icon */
        wf::signal::connection_t<wf::view_moved_to_wset_signal> onViewWSetChanged {
            [ = ] ( wf::view_moved_to_wset_signal *ev ) {
                wf::output_t *old_op = ev->old_wset->get_attached_output();
                wf::output_t *new_op = ev->new_wset->get_attached_output();

                uint32_t old_opid = (old_op == nullptr ? 0 : old_op->get_id() );
                uint32_t new_opid = (new_op == nullptr ? 0 : new_op->get_id() );

                /** Emit signal only if outputs are different */
                if ( old_opid != new_opid ) {
                    QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "ViewOutputChanged" );

                    msg << ev->view->get_id() << old_opid << new_opid;
                    QDBusConnection::sessionBus().send( msg );
                }

                /** TODO:
                 * Add support for multiple workspace sets.
                 */
            }
        };

        /**
         * Emitted when focus changes in an output.
         * Only emitted when wf::output::focus_view(...) is called.
         * Since the parameter wayfire_view can be mullptr, we should first check if the view is valid.
         */
        wf::signal::connection_t<wf::view_focus_request_signal> onViewFocused {
            [ = ] ( wf::view_focus_request_signal *ev ) {
                /**
                 * Should we emit a ViewFocusChanged on the stored view id?
                 * Check if we have a stored the old focused view id.
                 */
                if ( mOldFocusedViewId ) {
                    wayfire_view oldView = get_view_from_id( mOldFocusedViewId );

                    /* Okay, this view still exists */
                    if ( oldView ) {
                        bool shouldEmitSignal = true;

                        /** The view in the signal is valid and not the same as the old one. */
                        if ( ev->view ) {
                            shouldEmitSignal &= (ev->view->get_id() == mOldFocusedViewId);
                        }

                        if ( shouldEmitSignal ) {
                            QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "ViewFocusChanged" );

                            msg << mOldFocusedViewId << false;
                            QDBusConnection::sessionBus().send( msg );
                        }
                    }
                }

                /** The signal contains a new view */
                if ( ev->view ) {
                    /** Don't bother sending signals if background or layer shell surfaces gain focus */
                    if ( ev->view->role != wf::VIEW_ROLE_TOPLEVEL ) {
                        return;
                    }

                    auto toplevel = wf::toplevel_cast( ev->view );

                    /** The signalled view is activated => has keyboard focus */
                    if ( toplevel->activated ) {
                        QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "ViewFocusChanged" );

                        msg << toplevel->get_id() << true;
                        QDBusConnection::sessionBus().send( msg );

                        /** Store this id for future use */
                        mOldFocusedViewId = toplevel->get_id();
                    }
                }
            }
        };

        /* A view was minimized/restored - Show taskbar hint? */
        wf::signal::connection_t<wf::view_minimized_signal> onViewMinimized {
            [ = ] ( wf::view_minimized_signal *ev ) {
                QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "ViewMinimizedChanged" );

                msg << ev->view->get_id() << ev->view->minimized;
                QDBusConnection::sessionBus().send( msg );
            }
        };

        /* A view was maximized/restored - Show taskbar hint? */
        wf::signal::connection_t<wf::view_tiled_signal> onViewTiled {
            [ = ] ( wf::view_tiled_signal *ev ) {
                if ( ev->view->role != wf::VIEW_ROLE_TOPLEVEL ) {
                    return;
                }

                auto toplevel = wf::toplevel_cast( ev->view );

                bool         maximized = (toplevel->toplevel()->current().tiled_edges == wf::TILED_EDGES_ALL ? true : false);
                QDBusMessage msg       = QDBusMessage::createSignal( objPath, "wayland.compositor", "ViewMaximizedChanged" );

                msg << toplevel->get_id() << maximized;
                QDBusConnection::sessionBus().send( msg );
            }
        };

        /* A view was maximized/restored - Show taskbar hint? */
        wf::signal::connection_t<wf::view_set_sticky_signal> onViewStickied {
            [ = ] ( wf::view_set_sticky_signal *ev ) {
                QDBusMessage msg = QDBusMessage::createSignal( objPath, "wayland.compositor", "ViewStickyChanged" );

                msg << ev->view->get_id() << ev->view->sticky;
                QDBusConnection::sessionBus().send( msg );
            }
        };

    Q_SIGNALS:
        /** A new output was added to the compositor */
        void OutputAdded( uint output_id );

        /** An output was disconnected from the compositor */
        void OutputRemoved( uint output_id );

        /** Current output changed. @output_id is the new output */
        void OutputChanged( uint output_id );

        /** The current workspace of output @output_id changed */
        void OutputWorkspaceChanged( uint output_id, WorkSpace ws );

        /** The number of rows and columns of output @output_id changed */
        void OutputWorkspaceGridChanged( uint output_id, WorkSpaceGrid wsg );

        /** A view was added */
        void ViewAdded( uint view_id );

        /** A view was removed */
        void ViewRemoved( uint view_id );

        /** A view's output id changed */
        void ViewAppIdChanged( uint view_id, QString new_appid );

        /** A view's title changed */
        void ViewTitleChanged( uint view_id, QString new_title );

        /** A view gained/lost demands attention changed */
        void ViewAttentionChanged( uint view_id, bool attention );

        /** A view gained/lost focus */
        void ViewFocusChanged( uint view_id, bool focused );

        /** A view gained/lost sticky */
        void ViewStickyChanged( uint view_id, bool sticky );

        /** A view was minimized/restored */
        void ViewMinimizedChanged( uint view_id, bool minimized );

        /** A view was maximized/restored */
        void ViewMaximizedChanged( uint view_id, bool maximized );

        /** A view was fullscreened/restored */
        void ViewFullscreenChanged( uint view_id, bool fullscreen );

        /** A view was moved from one output to another */
        void ViewOutputMoved( uint view_id, uint oldOutput, uint newOutput );

        /** A view was moved from one workspace to another */
        void ViewWorkspaceChanged( uint view_id, WorkSpace from, WorkSpace to );
};
