/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QtCore>
#include <QtDBus>
#include <QDebug>

#include "dbustypes.hpp"

class OutputAdaptor : public QDBusAbstractAdaptor {
    Q_OBJECT;
    Q_CLASSINFO( "D-Bus Interface", "wayland.compositor.output" );

    Q_CLASSINFO( "D-Bus Introspection",
                 "<interface name='wayland.compositor.output'>\n"
                 "  <method name='ShowDesktop' />\n"
                 "  <method name='QueryOutputIds'>\n"
                 "    <arg type='au' direction='out'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.Out0' value='QUIntList'/>\n"
                 "  </method>\n"
                 "  <method name='QueryActiveOutput'>\n"
                 "    <arg type='u' direction='out'/>\n"
                 "  </method>\n"
                 "  <method name='QueryOutputWorkspaceGrid'>\n"
                 "    <arg type='(ii)' direction='out'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.Out0' value='WorkSpaceGrid'/>\n"
                 "    <arg name='output_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='QueryOutputWorkspace'>\n"
                 "    <arg type='(ii)' direction='out'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.Out0' value='WorkSpace'/>\n"
                 "    <arg name='output_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='ChangeWorkspace'>\n"
                 "    <arg name='output' type='u' direction='in'/>\n"
                 "    <arg name='ws' type='(ii)' direction='in'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.Out0' value='WorkSpace'/>\n"
                 "  </method>\n"
                 "  <method name='QueryActiveOutputViews'>\n"
                 "    <arg type='au' direction='out'/>\n"
                 "  </method>\n"
                 "  <method name='QueryOutputViews'>\n"
                 "    <arg name='output_id' type='u' direction='in'/>\n"
                 "    <arg type='au' direction='out'/>\n"
                 "  </method>\n"
                 "  <method name='PresentWorkspaceViews'>\n"
                 "    <arg name='output' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='PresentOutputViews'>\n"
                 "    <arg name='output' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "</interface>\n"
    );

    public:
        OutputAdaptor( QObject *parent );
        virtual ~OutputAdaptor();

        /** Show the desktop of the current workspace of the current output */
        Q_SLOT void ShowDesktop();

        /** List all the connected output IDs */
        Q_SLOT QUIntList QueryOutputIds();

        /** Query the currently active output */
        Q_SLOT uint QueryActiveOutput();

        /** Query the workspace grid size of the output @output_id */
        Q_SLOT WorkSpaceGrid QueryOutputWorkspaceGrid( uint output_id );

        /** Query the current workspace of the output @output_id */
        Q_SLOT WorkSpace QueryOutputWorkspace( uint output_id );

        /** Change the workspace of the given output @output_id */
        Q_SLOT void ChangeWorkspace( uint output, WorkSpace ws );

        /** List all the views on the current output */
        Q_SLOT QUIntList QueryActiveOutputViews();

        /** List all the views on the output @output_id */
        Q_SLOT QUIntList QueryOutputViews( uint outupt_id );

        /** Present all the windows of the output @output_id's current workspace */
        Q_SLOT void PresentWorkspaceViews( uint output_id );

        /** Present all the windows of the output @output_id */
        Q_SLOT void PresentOutputViews( uint output_id );

    private:
        bool mShowingDesktop = false;
};
