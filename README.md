
# DBusQt Plugin for Wayfire

This plugin exposes a few of Wayfire's functions and features to DBus.
This plugin is mainly built for use with DesQ
This project is heavily inspired by https://github.com/damianatorrpm/wayfire-plugin_dbus_interface.git

## Compile and install
You should have first compiled and installed wlroots, wf-config and wayfire.

- Get the sources
  - `git clone https://gitlab.com/wayfireplugins/dbusqt`
- Enter the `dbusqt`
  - `cd dbusqt`
- Configure the project - we use meson for project management
  - `meson build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  - `ninja -C build -k 0 -j $(nproc) && sudo ninja -C build install`

## Usage
This plugin creates the org.DesQ.Wayfire service for various uses.

### Examples
- To query top-level windows of the current workspace
    `qdbus --literal --session org.DesQ.Wayfire /org/Desq/Wayfire wayland.compositor.workspace.QueryActiveWorkspaceViews`

- To toggle fullscreen for a window `<view_id>` (query the id you want from the properties) org output <output_id> (0 for current)
    `qdbus --session org.DesQ.Wayfire /org/Desq/Wayfire wayland.compositor.ToggleFullscreenView <view_id> <output_id>`

- To list the workspace grid of the current output
    `qdbus --session org.DesQ.Wayfire /org/Desq/Wayfire wayland.compositor.workspace.QueryWorkspaceGrid`
