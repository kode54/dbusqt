/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include "Global.hpp"
#include "dbustypes.hpp"

class ViewsAdaptor : public QDBusAbstractAdaptor {
    Q_OBJECT;
    Q_CLASSINFO( "D-Bus Interface", "wayland.compositor.views" );

    Q_CLASSINFO( "D-Bus Introspection",
                 "<interface name='wayland.compositor.views'>\n"
                 "  <method name='QueryActiveView'>\n"
                 "    <arg type='u' direction='out'/>\n"
                 "  </method>\n"
                 "  <method name='CloseView'>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='FocusView'>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='ToggleFullscreenView'>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "    <arg name='output_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='ToggleMaximizeView'>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='ToggleMinimizeView'>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='ToggleStickyView'>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='QueryViewActive'>\n"
                 "    <arg type='b' direction='out'/>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='QueryViewAppId'>\n"
                 "    <arg type='s' direction='out'/>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='QueryViewAppIdGtkShell'>\n"
                 "    <arg type='s' direction='out'/>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='QueryViewPid'>\n"
                 "    <arg type='u' direction='out'/>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='QueryViewAttention'>\n"
                 "    <arg type='b' direction='out'/>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='QueryViewFullscreen'>\n"
                 "    <arg type='b' direction='out'/>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='QueryViewMaximized'>\n"
                 "    <arg type='b' direction='out'/>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='QueryViewMinimized'>\n"
                 "    <arg type='b' direction='out'/>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='QueryViewSticky'>\n"
                 "    <arg type='b' direction='out'/>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='QueryViewOutput'>\n"
                 "    <arg type='u' direction='out'/>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "  <method name='QueryViewTitle'>\n"
                 "    <arg type='s' direction='out'/>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "</interface>\n"
    );

    public:
        ViewsAdaptor( QObject *parent );
        virtual ~ViewsAdaptor();

    public Q_SLOTS:
        /** Query the active view - view with cursor focus */
        uint QueryActiveView();

        /** Close the view @view_id */
        void CloseView( uint view_id );

        /** Focus the view @view_id */
        void FocusView( uint view_id );

        /** Fullscreen the view @view_id on output @output_id.
         *  If output_id == 0, current output is used */
        void ToggleFullscreenView( uint view_id, uint output_id = 0 );

        /** Maximize/Restore the view @view_id */
        void ToggleMaximizeView( uint view_id );

        /** Minimize/Restore the view @view_id */
        void ToggleMinimizeView( uint view_id );

        /** Stick/Unstick the view @view_id */
        void ToggleStickyView( uint view_id );

        /** Query if the view @view_id is active */
        bool QueryViewActive( uint view_id );

        /** Query the app_id of view @view_id */
        QString QueryViewAppId( uint view_id );

        /** Query the app_id of view @view_id set by gtk shell */
        QString QueryViewAppIdGtkShell( uint view_id );

        /** Query the title of view @view_id */
        QString QueryViewTitle( uint view_id );

        /** Query the pid of view @view_id */
        uint QueryViewPid( uint view_id );

        /** Query if the view @view_id demands attention */
        bool QueryViewAttention( uint view_id );

        /** Query if the view @view_id is fullscreen */
        bool QueryViewFullscreen( uint view_id );

        /** Query if the view @view_id is maximzied */
        bool QueryViewMaximized( uint view_id );

        /** Query if the view @view_id minimized */
        bool QueryViewMinimized( uint view_id );

        /** Query if the view @view_id is sticky */
        bool QueryViewSticky( uint view_id );

        /** Query the output of view @view_id */
        uint QueryViewOutput( uint view_id );
};
