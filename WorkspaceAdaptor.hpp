/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include "Global.hpp"
#include "dbustypes.hpp"

class WorkspaceAdaptor : public QDBusAbstractAdaptor {
    Q_OBJECT;
    Q_CLASSINFO( "D-Bus Interface", "wayland.compositor.workspace" );

    Q_CLASSINFO( "D-Bus Introspection",
                 "<interface name='wayland.compositor.workspace'>\n"
                 "  <method name='QueryWorkspaceGrid'>\n"
                 "    <arg type='(ii)' direction='out'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.Out0' value='WorkSpaceGrid'/>\n"
                 "  </method>\n"
                 "  <method name='QueryActiveWorkspace'>\n"
                 "    <arg type='(ii)' direction='out'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.Out0' value='WorkSpace'/>\n"
                 "  </method>\n"
                 "  <method name='ChangeWorkspace'>\n"
                 "    <arg name='ws' type='(ii)' direction='in'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.Out0' value='WorkSpace'/>\n"
                 "  </method>\n"
                 "  <method name='PresentWorkspaceViews' />\n"
                 "  <method name='PresentActiveOutputViews' />\n"
                 "  <method name='MoveWithViewToWorkspace'>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "    <arg name='ws' type='(ii)' direction='in'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.In0' value='WorkSpace'/>\n"
                 "  </method>\n"
                 "  <method name='SendViewToWorkspace'>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "    <arg name='ws' type='(ii)' direction='in'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.In0' value='WorkSpace'/>\n"
                 "  </method>\n"
                 "  <method name='QueryActiveWorkspaceViews'>\n"
                 "    <arg type='au' direction='out'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.Out0' value='QUIntList'/>\n"
                 "  </method>\n"
                 "  <method name='QueryWorkspaceViews'>\n"
                 "    <arg type='au' direction='out'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.Out0' value='QUIntList'/>\n"
                 "    <arg name='ws' type='(ii)' direction='in'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.In0' value='WorkSpace'/>\n"
                 "  </method>\n"
                 "  <method name='QueryViewWorkspaces'>\n"
                 "    <arg type='a(ii)' direction='out'/>\n"
                 "    <annotation name='org.qtproject.QtDBus.QtTypeName.Out0' value='WorkSpaces'/>\n"
                 "    <arg name='view_id' type='u' direction='in'/>\n"
                 "  </method>\n"
                 "</interface>\n"
    );

    public:
        WorkspaceAdaptor( QObject *parent );
        virtual ~WorkspaceAdaptor();

    public Q_SLOTS:

        /** Query the WorkSpaceGrid of the active output */
        WorkSpaceGrid QueryWorkspaceGrid();

        /** Query the active WorkSpace of the active output */
        WorkSpace QueryActiveWorkspace();

        /** Change the WorkSpace of the active output */
        void ChangeWorkspace( WorkSpace ws );

        /** Present the current workspace views */
        void PresentWorkspaceViews();

        /** Present the active output's views */
        void PresentActiveOutputViews();

        /** Move view @view_id to workspace @ws - changes the active workspace */
        void MoveWithViewToWorkspace( uint view_id, WorkSpace ws );

        /** Send view @view_id to workspace @ws - retains the active workspace */
        void SendViewToWorkspace( uint view_id, WorkSpace ws );

        /** List all the views of the current workspace */
        QUIntList QueryActiveWorkspaceViews();

        /** List all the views of workspace @ws */
        QUIntList QueryWorkspaceViews( WorkSpace ws );

        /** Query the Workspaces on which the view @view_id is visible */
        WorkSpaces QueryViewWorkspaces( uint view_id );
};
