/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "dbus.hpp"
#include "MiscAdaptor.hpp"
#include "OutputAdaptor.hpp"
#include "WorkspaceAdaptor.hpp"
#include "ViewsAdaptor.hpp"

uint focused_view_id;
wf::compositor_core_t&      core       = wf::get_core();
std::vector<wf::output_t *> wf_outputs = core.output_layout->get_outputs();
std::set<wf::output_t *>    connected_wf_outputs;

QCoreApplication *app;

QString service = "org.DesQ.Wayfire";
QString objPath = "/org/DesQ/Wayfire";

wf::output_t * get_output_from_id( uint output_id ) {
    for ( wf::output_t *wf_output : wf_outputs ) {
        if ( wf_output->get_id() == output_id ) {
            return wf_output;
        }
    }

    return nullptr;
}


bool check_view_toplevel( wayfire_view view ) {
    if ( !view ) {
        return false;
    }

    if ( !view->is_mapped() ) {
        return false;
    }

    if ( view->role != wf::VIEW_ROLE_TOPLEVEL ) {
        return false;
    }

    if ( !view->get_output() ) {
        return false;
    }

    return true;
}


wayfire_toplevel_view get_view_from_id( uint view_id ) {
    std::vector<wayfire_view> view_vector;
    wayfire_view              view;

    view_vector = core.get_all_views();

    // there is no view_id 0 use it as get_active_view(hint)
    if ( view_id == 0 ) {
        view = core.get_cursor_focus_view();

        if ( check_view_toplevel( view ) ) {
            return wf::toplevel_cast( view );
        }
    }

    for (auto it = view_vector.begin(); it != view_vector.end(); ++it) {
        view = *it;

        if ( check_view_toplevel( view ) ) {
            if ( view->get_id() == view_id ) {
                return wf::toplevel_cast( view );
            }
        }
    }

    return wayfire_toplevel_view();
}


/* Convenience class to run our DBus Server */
void runDBusServer() {
    int  argc    = 1;
    char *argv[] = { (char *)"wayfire" };

    app = new QCoreApplication( argc, argv );

    WayfireDBusQt *dbusqt = new WayfireDBusQt();

    Q_UNUSED( dbusqt );

    app->exec();
}


/* Convenience class to close our DBus Server */
void closeDBusServer() {
    QDBusConnection::sessionBus().unregisterObject( objPath, QDBusConnection::UnregisterTree );
    QDBusConnection::sessionBus().unregisterService( service );

    app->quit();
}


WayfireDBusQt::WayfireDBusQt() {
    qDebug() << "WayfireDBusQt init";

    qRegisterMetaType<QUIntList>( "QUIntList" );
    qDBusRegisterMetaType<QUIntList>();

    qRegisterMetaType<WorkSpace>( "WorkSpace" );
    qDBusRegisterMetaType<WorkSpace>();

    qRegisterMetaType<WorkSpaces>( "WorkSpaces" );
    qDBusRegisterMetaType<WorkSpaces>();

    qRegisterMetaType<WorkSpaceGrid>( "WorkSpaceGrid" );
    qDBusRegisterMetaType<WorkSpaceGrid>();

    /** Initialize the DBus Adaptor for Misc functions and signals */
    new MiscAdaptor( this );

    /** Initialize the DBus Adaptor for Output related functions */
    new OutputAdaptor( this );

    /** Initialize the DBus Adaptor for Workspace related functions */
    new WorkspaceAdaptor( this );

    /** Initialize the DBus Adaptor for Views related functions */
    new ViewsAdaptor( this );

    wf::option_wrapper_t<std::string> service{ "dbusqt/service" };
    wf::option_wrapper_t<std::string> objPath{ "dbusqt/object" };

    QDBusConnection session = QDBusConnection::sessionBus();

    session.registerService( service.value().c_str() );
    session.registerObject( objPath.value().c_str(), this );
}


/* The plugin interface for WayfireDBusQtPlugin */
void WayfireDBusQtPlugin::init() {
    std::thread( runDBusServer ).detach();
}


void WayfireDBusQtPlugin::fini() {
    closeDBusServer();
}


DECLARE_WAYFIRE_PLUGIN( WayfireDBusQtPlugin );
